#!/bin/sh
####################################################################################
#
# Hello desu!^-^ I bet ur wondering why does this exist. Well as a grill with a lot of machines I like being able to run one thing to install all my crap. And github makes it easy to host so hopefully you wont mind? Tanky Wankys ~<3
# Pwease take this cute kitty ascii
#
#
#             *     ,MMM8&&&.            *
#                  MMMM88&&&&&    .
#                 MMMM88&&&&&&&
#     *           MMM88&&&&&&&&
#                 MMM88&&&&&&&&
#                 'MMM88&&&&&&'
#                   'MMM8&&&'      *
#          |\___/|
#          )     (             .              '
#         =\     /=
#           )===(       *
#          /     \
#          |     |
#         /       \
#         \       /
#  _/\_/\_/\__  _/_/\_/\_/\_/\_/\_/\_/\_/\_/\_
#  |  |  |  |( (  |  |  |  |  |  |  |  |  |  |
#  |  |  |  | ) ) |  |  |  |  |  |  |  |  |  |
#  |  |  |  |(_(  |  |  |  |  |  |  |  |  |  |
#  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
#  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
#
# https://github.com/Dynomite54/Fresh-Install/
#
####################################################################################

PS3='hello desu, please selcet one of the following: '
options=("Pacman" "bitchx" "dotnet" "Update" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "pacman")
			wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
			sudo apt-get install apt-transport-https
			echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
			sudo apt-get update
			sudo apt-get install sublime-text
			sudo apt-get install automake
			sudo apt-get install dirmngr
			sudo apt-get install build-essentials
			sudo apt-get install linux-headers-$(uname -r)
			sudo apt-get install compiz
			sudo apt-get install uuid-runtime
			sudo apt-get install guake
			sudo apt-get install terminator
			sudo apt-get install deluge
			sudo apt-get install gnome-illustrious-icon-theme
			sudo apt-get install curl network-manager-openvpn-gnome
			sudo apt-get install Python3-PIP
			sudo apt-get install IDLE
			sudo apt-get install rhythmbox
			sudo apt-get install vim
			exit
            ;;
        "bitchx")
			DOWNLOAD_URL=$(curl -s http://bitchx.sourceforge.net |\
			         grep "http://sourceforge.net" |\
			         sed -e "s|.*href=\"||g" |\
			         sed -e "s|\".*||g" |\
			         grep "/download" | uniq) # should only be one

			if [ "${DOWNLOAD_URL}" = "" ]; then
			  echo "ERROR: Could not find DOWNLOAD_URL from http://bitchx.sourceforge.net"
			  exit 255;
			fi
			# @todo make smarter, i.e. regexp, though now uses _always_ available commands (sic)
			VERSION=$(echo ${DOWNLOAD_URL} | sed -e "s|.*ircii-pana/bitchx-||g" | sed -e "s|\/.*||g")

			if [ "${VERSION}" = "" ]; then
			  echo "ERROR: Could not find VERSION from ${DOWNLOAD_URL}"
			  exit 255;
			fi
			echo "Will try to download and install version ${VERSION}";
			DOWNLOAD_URL=http://downloads.sourceforge.net/project/bitchx/ircii-pana/bitchx-${VERSION}/bitchx-${VERSION}.tar.gz
			echo "Downloading: ${DOWNLOAD_URL}"
			curl -L -s "${DOWNLOAD_URL}" -o bitchx-${VERSION}.tar.gz
			sudo apt-get install libssl-dev ncurses-dev
			sudo apt-get install automake
			tar -xzf bitchx-${VERSION}.tar.gz
			cd bitchx-${VERSION}
			./configure --prefix=/usr --with-ssl --with-plugins --enable-ipv6
			sudo bash autogen.sh
			make
			sudo make install
			cd $OLDPWD && sudo rm -rf bitchx-${VERSION}*
            ;;
        "dotnet")
			wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg
			sudo mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/
			wget -q https://packages.microsoft.com/config/debian/9/prod.list
			sudo mv prod.list /etc/apt/sources.list.d/microsoft-prod.list
			sudo chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg
			sudo chown root:root /etc/apt/sources.list.d/microsoft-prod.list
			sudo apt-get update
			sudo apt-get install aspnetcore-runtime-2.1
            ;;
        "monodev")
			sudo apt install apt-transport-https dirmngr
			sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
			echo "deb https://download.mono-project.com/repo/debian vs-stretch main" | sudo tee /etc/apt/sources.list.d/mono-official-vs.list
			sudo apt update
			sudo apt-get install monodevelop
			;;
        "update")
			sudo apt-get update
			sudo apt-get upgrade
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
